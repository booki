import models
from django.contrib import admin

admin.site.register(models.License)
admin.site.register(models.Project)
admin.site.register(models.Book)
admin.site.register(models.Info)
admin.site.register(models.Chapter)
admin.site.register(models.Attachment)
admin.site.register(models.Language)
admin.site.register(models.ProjectStatus)
admin.site.register(models.BookToc)
